import React, { useState } from 'react'
import logo from './logo.svg';
import './App.css';
const axios = require('axios');
function App() {
  const [datas, setDatas] = useState("");
  const handleCallApi = async () => {
    try {
      const response = await axios.get('http://52.221.191.194:8080/books');
      let dataStr = JSON.stringify(response.data);
      setDatas(dataStr);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <button onClick={() => handleCallApi()}>
          Call API
        </button>
        <br />
        <div>Display</div>
        <div>
          <pre>
            <code>{datas}
            </code>
          </pre>
        </div>
      </header>
    </div >
  );
}

export default App;
